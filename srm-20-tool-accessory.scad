ToolCaseSize = 13.5;
ToolCasePocketDepth = 20;
HexKeyDiameter = 3;
Spacing = 3;

// This to define the polygon outline
FinalSizeXa = (ToolCaseSize * 3) + HexKeyDiameter + (Spacing * 5);
FinalSizeXb = (ToolCaseSize * 3) + (Spacing * 4);
FinalSizeYa = HexKeyDiameter + (Spacing * 2);
FinalSizeYb = ToolCaseSize + (Spacing * 2);

// This to define positions of pockets and holes
HexKeyHolePosition = [
    FinalSizeXa - Spacing - (HexKeyDiameter / 2),
    FinalSizeYa - Spacing - (HexKeyDiameter / 2)];

difference(){
    DrawOuterForm();
    DrawAllToolCasePockets();
}

module DrawOuterForm(){
    linear_extrude(height = ToolCasePocketDepth + Spacing, center = false){
        difference(){
            DrawOutline();
            DrawHexKeyHole();
        }
    }
}

module DrawOutline(){
    polygon(points = [
        [0, 0], 
        [FinalSizeXa, 0], 
        [FinalSizeXa, FinalSizeYa], 
        [FinalSizeXb, FinalSizeYb],
        [0, FinalSizeYb]]);
}

module DrawHexKeyHole(){
    translate(HexKeyHolePosition)
        circle(r = HexKeyDiameter / 2, $fn = 36);
}

module DrawAllToolCasePockets(){
    translate([0, 0, Spacing]){
        translate([Spacing, Spacing]) DrawToolCasePocket();
        translate([(Spacing * 2) + ToolCaseSize, Spacing]) DrawToolCasePocket();
        translate([(Spacing * 3) + (ToolCaseSize * 2), Spacing]) DrawToolCasePocket();
    }
}

module DrawToolCasePocket(){
    cube(
        size = [
            ToolCaseSize, 
            ToolCaseSize, 
            ToolCasePocketDepth + 0.01], // 0,01 is a hack for preview
        center = false);
}
